import { writable } from 'svelte/store';

export const langue = writable('fr');
export const dyslexie = writable(false);
